<?php
include('config.php');

$q = mysqli_query($con,"SELECT * FROM st_campaign WHERE is_stopped=0 AND type=1");
$n_rows = mysqli_num_rows($q);

if($n_rows>0){
	$camp_id_array = array();
	while($row=mysqli_fetch_array($q)){
		$c_id = $row['id'];
		array_push($camp_id_array,$c_id);
	}
	
	//getting running campaigns count
	$array_count = count($camp_id_array);
	$array_count = $array_count - 1;
	
	//getting random index and random campaign id
	$random_index = rand(0,$array_count);
	$random_campaign_id = $camp_id_array[$random_index];
	
	$q = mysqli_query($con,"SELECT * FROM st_campaign WHERE id=$random_campaign_id");
	$row = mysqli_fetch_array($q);
	$responsible_agents = $row['responsible_agents'];
	
	//find out available agents
	$q = mysqli_query($con,"SELECT * FROM st_sip_users WHERE sip_exten IN ($responsible_agents) AND peer_status='reachable' AND device_state_change='not_inuse'");
	$n_rows = mysqli_num_rows($q);
	if($n_rows>0){
		$agents_array = array();
		while($row=mysqli_fetch_array($q)){
			$sip_exten = $row['sip_exten'];
			array_push($agents_array,$sip_exten);
		}
		$agents_array_count = count($agents_array);
		
		//getting data from outbound table to dial out
		$q = mysqli_query($con,"SELECT * FROM st_outbound WHERE campaign_id=$random_campaign_id AND is_picked_up=0 AND is_dialed=0 LIMIT $agents_array_count");
		$n_rows = mysqli_num_rows($q);
		if($n_rows>0){
			//process call file here for the record
			$i = 0;
			$ids = "";
			while($row=mysqli_fetch_array($q)){
				$ids .= $row['id'].",";
				$outbound_id = $row['id'];
				$number = $row['number'];
				$campaign_id = $row['campaign_id'];
				$client_id = $row['client_id'];
				$outbound_caller_id = $row['outbound_caller_id'];
				$welcome_greeting = $row['welcome_greeting'];
				$press_digit = $row['press_digit'];
				$queue = $row['queue'];
				$timeout = $row['timeout'];
				$moh_name = $row['moh_name'];
				$call_record = $row['call_record'];
				$agent = $agents_array[$i];
				
				$uniqid = uniqid();
				$myfile = fopen(FAXPATH.$uniqid.".call", "w") or die("Unable to open file!");
			
				$channel = "Channel: PJSIP/".$number."@mtntrunk"."\n";
				$callerid = "Callerid: ".$outbound_caller_id."\n";
				$context = "Context: outbound-broadcast"."\n";
				$extension = "Extension: s"."\n";
				$number = "Setvar: number=".$number."\n";
				$agent = "Setvar: agent=".$agent."\n";
				$campaign_id = "Setvar: campaign_id=".$campaign_id."\n";
				$client_id = "Setvar: client_id=".$client_id."\n";
				$outbound_caller_id = "Setvar: outbound_caller_id=".$outbound_caller_id."\n";
				$welcome_greeting = "Setvar: welcome_greeting=".$welcome_greeting."\n";
				$press_digit = "Setvar: press_digit=".$press_digit."\n";
				$queue = "Setvar: queue=".$queue."\n";
				$timeout = "Setvar: timeout=".$timeout."\n";
				$moh_name = "Setvar: moh_name=".$moh_name."\n";
				$call_record = "Setvar: call_record=".$call_record."\n";
				$outbound_id = "Setvar: outbound_id=".$outbound_id."\n";
				
				fwrite($myfile, $channel);
				fwrite($myfile, $callerid);
				fwrite($myfile, $context);
				fwrite($myfile, $extension);
				fwrite($myfile, $number);
				fwrite($myfile, $agent);
				fwrite($myfile, $campaign_id);
				fwrite($myfile, $client_id);
				fwrite($myfile, $outbound_caller_id);
				fwrite($myfile, $welcome_greeting);
				fwrite($myfile, $press_digit);
				fwrite($myfile, $queue);
				fwrite($myfile, $timeout);
				fwrite($myfile, $moh_name);
				fwrite($myfile, $call_record);
				fwrite($myfile, $outbound_id);
				
				fclose($myfile);
				
				$call_file = FAXPATH.$uniqid.".call"; // call file name to move in outgoing directory
				
				`mv $call_file /var/spool/asterisk/outgoing/`;
				$i++;
			}
			
			//updating flag for picked up records
			$ids = rtrim($ids,",");
			//$q = "UPDATE st_outbound SET is_picked_up=1 WHERE id IN ($ids)";
			//mysqli_query($con,$q);
		}
		else{
			echo "No record available to process for the campaign\n";
			$q = "UPDATE st_campaign SET is_stopped=1 WHERE id=$random_campaign_id";
			mysqli_query($con,$q);
		}
	}
	else{
		echo "No agents available to talk\n";
	}
	
}
else{
	echo "No campaign started\n";
}

?>