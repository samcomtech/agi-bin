#!/usr/bin/php
<?php
include('config.php');
$callid = $argv[1];
$callerid = $argv[2];
$callee = $argv[3];

global $agi,$prompts;
require('phpagi/phpagi.php');
$agi = new AGI();

$agi->verbose("****callid is $callid caller id is $callerid callee is $callee *******");

$q = mysqli_query($con,"SELECT * FROM st_phone_number WHERE did_number=$callee AND status=1");
$n_rows = mysqli_num_rows($q);
if($n_rows>0){
	//fetching DID number realted properties
	$row = mysqli_fetch_array($q);
	$tracking_source_id = $row['tracking_source_id'];$reseller_id = $row['reseller_id'];$client_id = $row['client_id'];
	$user_id = $row['user_id'];$is_business_hours = $row['is_business_hours'];$call_record = $row['call_record'];
	$training_quality_enabled = $row['training_quality_enabled'];$training_quality_message = $row['training_quality_message'];
	$moh_enabled = $row['moh_enabled'];$moh_name = $row['moh_name'];
	$welcome_enabled = $row['welcome_enabled'];$welcome_message = $row['welcome_message'];$ivr_message = $row['ivr_message'];
	$after_ivr_message = $row['after_ivr_message'];$main_action = $row['main_action'];$extension = $row['extension'];
	$department = $row['department'];$phone = $row['phone'];$message = $row['message'];$voicemail_enabled = $row['voicemail_enabled'];
	$voicemail_message = $row['voicemail_message'];$voicemail_extension = $row['voicemail_extension'];$queue = $row['queue'];$conference = $row['conference'];$ivr = $row['ivr'];
	$after_ivr = $row['after_ivr'];$after_hour_action = $row['after_hour_action'];$after_extension = $row['after_extension'];
	$after_department = $row['after_department'];$after_phone = $row['after_phone'];$after_message = $row['after_message'];
	$after_voicemail_message = $row['after_voicemail_message'];$after_voicemail_enabled = $row['after_voicemail_enabled'];$after_voicemail_extension = $row['after_voicemail_extension'];$after_queue = $row['after_queue'];
	$after_conference = $row['after_conference'];$invalid_message = $row['invalid_message'];$timeout_message = $row['timeout_message'];$timeout = $row['timeout'];$fax_enabled = $row['fax_enabled'];
	
	if($fax_enabled==1){
		$creationdate = date('Y-m-d H:i:s');
		$uniqueid = uniqid();
		$fax_tiff = $uniqueid.".tiff";
		$fax_pdf = $uniqueid.".pdf";
		$q = "INSERT INTO fax_history (client_id,creationdate,uuid,sender,receiver,fax_tiff,fax_pdf,direction) VALUES ($client_id,'$creationdate','$uniqueid','$callerid','$callee','$fax_tiff','$fax_pdf','inbound')";
		mysqli_query($con,$q);
		$agi->set_variable("uniq", $uniqueid);
		$agi->set_variable("faxpath", FAXPATH);
		$agi->exec("Goto","receive-fax,s,1");
	}
	else{
		//setting dialplan variables
	$agi->set_variable("tracking_source_id", $tracking_source_id);$agi->set_variable("reseller_id", $reseller_id);$agi->set_variable("client_id", $client_id);
	$agi->set_variable("user_id", $user_id);$agi->set_variable("is_business_hours", $is_business_hours);$agi->set_variable("call_record", $call_record);
	$agi->set_variable("training_quality_enabled", $training_quality_enabled);$agi->set_variable("training_quality_message", $training_quality_message);
	$agi->set_variable("moh_enabled", $moh_enabled);$agi->set_variable("moh_name", $moh_name);
	$agi->set_variable("welcome_enabled", $welcome_enabled);$agi->set_variable("welcome_message", $welcome_message);$agi->set_variable("ivr_message", $ivr_message);
	$agi->set_variable("after_ivr_message", $after_ivr_message);$agi->set_variable("main_action", $main_action);$agi->set_variable("extension", $extension);
	$agi->set_variable("department", $department);$agi->set_variable("phone", $phone);$agi->set_variable("message", $message);$agi->set_variable("voicemail_enabled", $voicemail_enabled);
	$agi->set_variable("voicemail_message", $voicemail_message);$agi->set_variable("voicemail_extension", $voicemail_extension);$agi->set_variable("queue", $queue);
	$agi->set_variable("conference", $conference);$agi->set_variable("ivr", $ivr);
	$agi->set_variable("after_ivr", $after_ivr);$agi->set_variable("after_hour_action", $after_hour_action);$agi->set_variable("after_extension", $after_extension);
	$agi->set_variable("after_department", $after_department);$agi->set_variable("after_phone", $after_phone);$agi->set_variable("after_message", $after_message);
	$agi->set_variable("after_voicemail_message", $after_voicemail_message);$agi->set_variable("after_voicemail_enabled", $after_voicemail_enabled);$agi->set_variable("after_voicemail_extension", $after_voicemail_extension);
	$agi->set_variable("after_queue", $after_queue);$agi->set_variable("after_conference", $after_conference);
	$agi->set_variable("invalid_message", $invalid_message);$agi->set_variable("timeout_message", $timeout_message);
	$agi->set_variable("timeout", $timeout);
	
	//getting full path of message filename without extension
	$welcome_message = PROMPTPATH.$welcome_message;$welcome_message = basename($welcome_message,".wav");
	$voicemail_message = PROMPTPATH.$voicemail_message;$voicemail_message = basename($voicemail_message,".wav");
	$message = PROMPTPATH.$message;$message = basename($message,".wav");
	$after_message = PROMPTPATH.$after_message;$after_message = basename($after_message,".wav");
	$ivr_message = PROMPTPATH.$ivr_message;$ivr_message = basename($ivr_message,".wav");
	$invalid_message = PROMPTPATH.$invalid_message;$invalid_message = basename($invalid_message,".wav");
	$timeout_message = PROMPTPATH.$timeout_message;$timeout_message = basename($timeout_message,".wav");
	$after_voicemail_message = PROMPTPATH.$after_voicemail_message;$after_voicemail_message = basename($after_voicemail_message,".wav");
	$after_ivr_message = PROMPTPATH.$after_ivr_message;$after_ivr_message = basename($after_ivr_message,".wav");
	$training_quality_message = PROMPTPATH.$training_quality_message;$training_quality_message = basename($training_quality_message,".wav");
	
	//initial call log query
	$call_start = date('Y-m-d H:i:s');
	$q = "INSERT INTO st_call_logs (call_id,user_id,client_id,tracking_source_id,caller_id,callee,direction,call_start) VALUES 
	('$callid',$user_id,$client_id,$tracking_source_id,$callerid,$callee,'inbound','$call_start')";
	$agi->verbose("INITIAL CALL LOG QUERY: $q ");
	mysqli_query($con,$q);
	
	//checking business hours
	if($is_business_hours==1){
		//check business time here
		$q = mysqli_query($con,"SELECT * FROM st_phone_number_timings WHERE did_number=$callee");
		$row = mysqli_fetch_array($q);
		$day = $row['day'];$start_time = $row['start_time'];$end_time = $row['end_time'];
		$current_day = date('l');$current_time = date('H:i:s');
		
		if($current_day==$day&&($current_time>=$start_time&&$current_time<=$end_time)){
			//in business hours
			$agi->verbose("**** IN BUSINESS HOURS *******");
			if($main_action=="EXTENSION"){
				if($welcome_enabled==1){
				if($call_record==1){
					$agi->exec("Playback",PROMPTPATH.$welcome_message);
					$agi->exec("MixMonitor",$callid.".wav,ab");
					if($training_quality_enabled==1){$agi->exec("Playback",PROMPTPATH.$training_quality_message);}
					if($moh_enabled==1){
						$agi->exec("Dial","PJSIP/".$extension.",".$timeout.",m(".$moh_name.")");
					}
					else{
						$agi->exec("Dial","PJSIP/".$extension.",".$timeout);
					}
					
					
					$res = $agi->get_variable('DIALSTATUS');
					$resultcode = $res['result'];
					$status = $res['data'];//dialstatus checking
					$agi->verbose("**** CALL STATUS , $status AND RESULT CODE $resultcode *******");
					if($voicemail_enabled==1&&($status=="NOANSWER"||$status=="BUSY")){
						$agi->exec("Playback",PROMPTPATH.$voicemail_message);
						$agi->exec("Voicemail",$voicemail_extension."@voicemail-".$client_id.",s");
						$agi->hangup();
					}
					else{
						$agi->hangup();
					}
				}
				else{
					$agi->exec("Playback",PROMPTPATH.$welcome_message);
					if($training_quality_enabled==1){$agi->exec("Playback",PROMPTPATH.$training_quality_message);}
					if($moh_enabled==1){
						$agi->exec("Dial","PJSIP/".$extension.",".$timeout.",m(".$moh_name.")");
					}
					else{
						$agi->exec("Dial","PJSIP/".$extension.",".$timeout);
					}
					
					$res = $agi->get_variable('DIALSTATUS');
					$resultcode = $res['result'];
					$status = $res['data'];//dialstatus checking
					$agi->verbose("**** CALL STATUS , $status AND RESULT CODE $resultcode *******");
					if($voicemail_enabled==1&&($status=="NOANSWER"||$status=="BUSY")){
						$agi->exec("Playback",PROMPTPATH.$voicemail_message);
						$agi->exec("Voicemail",$voicemail_extension."@voicemail-".$client_id.",s");
						$agi->hangup();
					}
					else{
						$agi->hangup();
					}
				}
			}
			else{
				if($call_record==1){
					$agi->exec("MixMonitor",$callid.".wav,ab");
					if($training_quality_enabled==1){$agi->exec("Playback",PROMPTPATH.$training_quality_message);}
					if($moh_enabled==1){
						$agi->exec("Dial","PJSIP/".$extension.",".$timeout.",m(".$moh_name.")");
					}
					else{
						$agi->exec("Dial","PJSIP/".$extension.",".$timeout);
					}
					
					$res = $agi->get_variable('DIALSTATUS');
					$resultcode = $res['result'];
					$status = $res['data'];//dialstatus checking
					$agi->verbose("**** CALL STATUS , $status AND RESULT CODE $resultcode *******");
					if($voicemail_enabled==1&&($status=="NOANSWER"||$status=="BUSY")){
						$agi->exec("Playback",PROMPTPATH.$voicemail_message);
						$agi->exec("Voicemail",$voicemail_extension."@voicemail-".$client_id.",s");
						$agi->hangup();
					}
					else{
						$agi->hangup();
					}
				}
				else{
					if($training_quality_enabled==1){$agi->exec("Playback",PROMPTPATH.$training_quality_message);}
					if($moh_enabled==1){
						$agi->exec("Dial","PJSIP/".$extension.",".$timeout.",m(".$moh_name.")");
					}
					else{
						$agi->exec("Dial","PJSIP/".$extension.",".$timeout);
					}
					
					$res = $agi->get_variable('DIALSTATUS');
					$resultcode = $res['result'];
					$status = $res['data'];//dialstatus checking
					$agi->verbose("**** CALL STATUS , $status AND RESULT CODE $resultcode *******");
					if($voicemail_enabled==1&&($status=="NOANSWER"||$status=="BUSY")){
						$agi->exec("Playback",PROMPTPATH.$voicemail_message);
						$agi->exec("Voicemail",$voicemail_extension."@voicemail-".$client_id.",s");
						$agi->hangup();
					}
					else{
						$agi->hangup();
					}
				}
				
			}
			}
			elseif($main_action=="DEPARTMENT"){
			$qdept = mysqli_query($con,"SELECT * FROM st_department WHERE id=$department");
			$rowdept = mysqli_fetch_array($qdept);
			$department = $rowdept['json_string'];
			if($welcome_enabled==1){
				if($call_record==1){
					$agi->exec("Playback",PROMPTPATH.$welcome_message);
					$agi->exec("MixMonitor",$callid.".wav,ab");
					$agi->exec("Set","CALLERID(num)=".$callee);
					if($training_quality_enabled==1){$agi->exec("Playback",PROMPTPATH.$training_quality_message);}
					if($moh_enabled==1){
						$agi->exec("Dial",$department.",".$timeout.",m(".$moh_name.")");
					}
					else{
						$agi->exec("Dial",$department.",".$timeout);
					}
					
					
					$res = $agi->get_variable('DIALSTATUS');
					$resultcode = $res['result'];
					$status = $res['data'];//dialstatus checking
					$agi->verbose("**** CALL STATUS , $status AND RESULT CODE $resultcode *******");
					if($voicemail_enabled==1&&($status=="NOANSWER"||$status=="BUSY")){
						$agi->exec("Playback",PROMPTPATH.$voicemail_message);
						$agi->exec("Voicemail",$voicemail_extension."@voicemail-".$client_id.",s");
						$agi->hangup();
					}
					else{
						$agi->hangup();
					}
				}
				else{
					$agi->exec("Playback",PROMPTPATH.$welcome_message);
					$agi->exec("Set","CALLERID(num)=".$callee);
					if($training_quality_enabled==1){$agi->exec("Playback",PROMPTPATH.$training_quality_message);}
					if($moh_enabled==1){
						$agi->exec("Dial",$department.",".$timeout.",m(".$moh_name.")");
					}
					else{
						$agi->exec("Dial",$department.",".$timeout);
					}
					
					$res = $agi->get_variable('DIALSTATUS');
					$resultcode = $res['result'];
					$status = $res['data'];//dialstatus checking
					$agi->verbose("**** CALL STATUS , $status AND RESULT CODE $resultcode *******");
					if($voicemail_enabled==1&&($status=="NOANSWER"||$status=="BUSY")){
						$agi->exec("Playback",PROMPTPATH.$voicemail_message);
						$agi->exec("Voicemail",$voicemail_extension."@voicemail-".$client_id.",s");
						$agi->hangup();
					}
					else{
						$agi->hangup();
					}
				}
			}
			else{
				if($call_record==1){
					$agi->exec("MixMonitor",$callid.".wav,ab");
					$agi->exec("Set","CALLERID(num)=".$callee);
					if($training_quality_enabled==1){$agi->exec("Playback",PROMPTPATH.$training_quality_message);}
					if($moh_enabled==1){
						$agi->exec("Dial",$department.",".$timeout.",m(".$moh_name.")");
					}
					else{
						$agi->exec("Dial",$department.",".$timeout);
					}
					
					$res = $agi->get_variable('DIALSTATUS');
					$resultcode = $res['result'];
					$status = $res['data'];//dialstatus checking
					$agi->verbose("**** CALL STATUS , $status AND RESULT CODE $resultcode *******");
					if($voicemail_enabled==1&&($status=="NOANSWER"||$status=="BUSY")){
						$agi->exec("Playback",PROMPTPATH.$voicemail_message);
						$agi->exec("Voicemail",$voicemail_extension."@voicemail-".$client_id.",s");
						$agi->hangup();
					}
					else{
						$agi->hangup();
					}
				}
				else{
					$agi->exec("Set","CALLERID(num)=".$callee);
					if($training_quality_enabled==1){$agi->exec("Playback",PROMPTPATH.$training_quality_message);}
					if($moh_enabled==1){
						$agi->exec("Dial",$department.",".$timeout.",m(".$moh_name.")");
					}
					else{
						$agi->exec("Dial",$department.",".$timeout);
					}
					
					$res = $agi->get_variable('DIALSTATUS');
					$resultcode = $res['result'];
					$status = $res['data'];//dialstatus checking
					$agi->verbose("**** CALL STATUS , $status AND RESULT CODE $resultcode *******");
					if($voicemail_enabled==1&&($status=="NOANSWER"||$status=="BUSY")){
						$agi->exec("Playback",PROMPTPATH.$voicemail_message);
						$agi->exec("Voicemail",$voicemail_extension."@voicemail-".$client_id.",s");
						$agi->hangup();
					}
					else{
						$agi->hangup();
					}
				}
			}
		}
		elseif($main_action=="PHONE"){
			if($welcome_enabled==1){
				if($call_record==1){
					$agi->exec("Playback",PROMPTPATH.$welcome_message);
					$agi->exec("MixMonitor",$callid.".wav,ab");
					$agi->exec("Set","CALLERID(num)=".$callee);
					if($training_quality_enabled==1){$agi->exec("Playback",PROMPTPATH.$training_quality_message);}
					if($moh_enabled==1){
						$agi->exec("Dial","PJSIP/".$phone."@mtntrunk,".$timeout.",m(".$moh_name.")");
					}
					else{
						$agi->exec("Dial","PJSIP/".$phone."@mtntrunk,".$timeout);
					}
					
					
					$res = $agi->get_variable('DIALSTATUS');
					$resultcode = $res['result'];
					$status = $res['data'];//dialstatus checking
					$agi->verbose("**** CALL STATUS , $status AND RESULT CODE $resultcode *******");
					if($voicemail_enabled==1&&($status=="NOANSWER"||$status=="BUSY")){
						$agi->exec("Playback",PROMPTPATH.$voicemail_message);
						$agi->exec("Voicemail",$voicemail_extension."@voicemail-".$client_id.",s");
						$agi->hangup();
					}
					else{
						$agi->hangup();
					}
				}
				else{
					$agi->exec("Playback",PROMPTPATH.$welcome_message);
					$agi->exec("Set","CALLERID(num)=".$callee);
					if($training_quality_enabled==1){$agi->exec("Playback",PROMPTPATH.$training_quality_message);}
					if($moh_enabled==1){
						$agi->exec("Dial","PJSIP/".$phone."@mtntrunk,".$timeout.",m(".$moh_name.")");
					}
					else{
						$agi->exec("Dial","PJSIP/".$phone."@mtntrunk,".$timeout);
					}
					
					$res = $agi->get_variable('DIALSTATUS');
					$resultcode = $res['result'];
					$status = $res['data'];//dialstatus checking
					$agi->verbose("**** CALL STATUS , $status AND RESULT CODE $resultcode *******");
					if($voicemail_enabled==1&&($status=="NOANSWER"||$status=="BUSY")){
						$agi->exec("Playback",PROMPTPATH.$voicemail_message);
						$agi->exec("Voicemail",$voicemail_extension."@voicemail-".$client_id.",s");
						$agi->hangup();
					}
					else{
						$agi->hangup();
					}
				}
			}
			else{
				if($call_record==1){
					$agi->exec("MixMonitor",$callid.".wav,ab");
					$agi->exec("Set","CALLERID(num)=".$callee);
					if($training_quality_enabled==1){$agi->exec("Playback",PROMPTPATH.$training_quality_message);}
					if($moh_enabled==1){
						$agi->exec("Dial","PJSIP/".$phone."@mtntrunk,".$timeout.",m(".$moh_name.")");
					}
					else{
						$agi->exec("Dial","PJSIP/".$phone."@mtntrunk,".$timeout);
					}
					
					$res = $agi->get_variable('DIALSTATUS');
					$resultcode = $res['result'];
					$status = $res['data'];//dialstatus checking
					$agi->verbose("**** CALL STATUS , $status AND RESULT CODE $resultcode *******");
					if($voicemail_enabled==1&&($status=="NOANSWER"||$status=="BUSY")){
						$agi->exec("Playback",PROMPTPATH.$voicemail_message);
						$agi->exec("Voicemail",$voicemail_extension."@voicemail-".$client_id.",s");
						$agi->hangup();
					}
					else{
						$agi->hangup();
					}
				}
				else{
					$agi->exec("Set","CALLERID(num)=".$callee);
					if($training_quality_enabled==1){$agi->exec("Playback",PROMPTPATH.$training_quality_message);}
					if($moh_enabled==1){
						$agi->exec("Dial","PJSIP/".$phone."@mtntrunk,".$timeout.",m(".$moh_name.")");
					}
					else{
						$agi->exec("Dial","PJSIP/".$phone."@mtntrunk,".$timeout);
					}
					
					$res = $agi->get_variable('DIALSTATUS');
					$resultcode = $res['result'];
					$status = $res['data'];//dialstatus checking
					$agi->verbose("**** CALL STATUS , $status AND RESULT CODE $resultcode *******");
					if($voicemail_enabled==1&&($status=="NOANSWER"||$status=="BUSY")){
						$agi->exec("Playback",PROMPTPATH.$voicemail_message);
						$agi->exec("Voicemail",$voicemail_extension."@voicemail-".$client_id.",s");
						$agi->hangup();
					}
					else{
						$agi->hangup();
					}
				}
			}
		}
		elseif($main_action=="MESSAGE"){
			if($welcome_enabled==1){
				$agi->exec("Playback",PROMPTPATH.$welcome_message);
				$agi->exec("Playback",PROMPTPATH.$message);
			}
			else{
				$agi->exec("Playback",PROMPTPATH.$message);
			}
		}
		elseif($main_action=="VOICEMAIL"){
			if($welcome_enabled==1){
				$agi->exec("Playback",PROMPTPATH.$welcome_message);
				$agi->exec("Playback",PROMPTPATH.$voicemail_message);
				$agi->exec("Voicemail",$voicemail_extension."@voicemail-".$client_id.",s");
				$agi->hangup();
			}
			else{
				$agi->exec("Playback",PROMPTPATH.$voicemail_message);
				$agi->exec("Voicemail",$voicemail_extension."@voicemail-".$client_id.",s");
				$agi->hangup();
			}
		}
		elseif($main_action=="DIRECTORY"){
			
		}
		elseif($main_action=="IVR"){
			$anotherivr = 0;
			$timeoutcount = 0;$invalidcount = 0;
			$anothertimeoutcount = 0;$anotherinvalidcount = 0;
			$ivr = json_decode($ivr,true);
			ivr_again:
			if($welcome_enabled==1){$agi->exec("Playback",PROMPTPATH.$welcome_message);}
			if($call_record==1){$agi->exec("MixMonitor",$callid.".wav,ab");}
			
			in_hours_ivr_to_ivr:
			$result = $agi->get_data(PROMPTPATH.$ivr_message, 5000, 1);
			$digit = $result['result'];
			$ivrdata = $result['data'];
			$press_key = "press".$digit;
			
			if(array_key_exists($press_key,$ivr)){
				$agi->verbose("**** $press_key *******");
				$app = $ivr[$press_key]['App'];
				$data = $ivr[$press_key]['Data'];
				
				if($app=="EXTENSION"){
					if($training_quality_enabled==1){$agi->exec("Playback",PROMPTPATH.$training_quality_message);}
					if($moh_enabled==1){
						$agi->exec("Dial","PJSIP/".$data.",".$timeout.",m(".$moh_name.")");
					}
					else{
						$agi->exec("Dial","PJSIP/".$data.",".$timeout);
					}
					
					
					$res = $agi->get_variable('DIALSTATUS');
					$resultcode = $res['result'];
					$status = $res['data'];//dialstatus checking
					$agi->verbose("**** CALL STATUS , $status AND RESULT CODE $resultcode *******");
					if($voicemail_enabled==1&&($status=="NOANSWER"||$status=="BUSY")){
						$agi->exec("Playback",PROMPTPATH.$voicemail_message);
						$agi->exec("Voicemail",$voicemail_extension."@voicemail-".$client_id.",s");
						$agi->hangup();
					}
					else{
						$agi->hangup();
					}
				}
				elseif($app=="DEPARTMENT"){
					$qdept = mysqli_query($con,"SELECT * FROM st_department WHERE id=$data");
					$rowdept = mysqli_fetch_array($qdept);
					$data = $rowdept['json_string'];
					$agi->exec("Set","CALLERID(num)=".$callee);
					if($training_quality_enabled==1){$agi->exec("Playback",PROMPTPATH.$training_quality_message);}
					if($moh_enabled==1){
						$agi->exec("Dial",$data.",".$timeout.",m(".$moh_name.")");
					}
					else{
						$agi->exec("Dial",$data.",".$timeout);
					}
					
					
					$res = $agi->get_variable('DIALSTATUS');
					$resultcode = $res['result'];
					$status = $res['data'];//dialstatus checking
					$agi->verbose("**** CALL STATUS , $status AND RESULT CODE $resultcode *******");
					if($voicemail_enabled==1&&($status=="NOANSWER"||$status=="BUSY")){
						$agi->exec("Playback",PROMPTPATH.$voicemail_message);
						$agi->exec("Voicemail",$voicemail_extension."@voicemail-".$client_id.",s");
						$agi->hangup();
					}
					else{
						$agi->hangup();
					}
				}
				elseif($app=="PHONE"){
					$agi->exec("Set","CALLERID(num)=".$callee);
					if($training_quality_enabled==1){$agi->exec("Playback",PROMPTPATH.$training_quality_message);}
					if($moh_enabled==1){
						$agi->exec("Dial","PJSIP/".$data."@mtntrunk,",$timeout.",m(".$moh_name.")");
					}
					else{
						$agi->exec("Dial","PJSIP/".$data."@mtntrunk,",$timeout);
					}
					
					
					$res = $agi->get_variable('DIALSTATUS');
					$resultcode = $res['result'];
					$status = $res['data'];//dialstatus checking
					$agi->verbose("**** CALL STATUS , $status AND RESULT CODE $resultcode *******");
					if($voicemail_enabled==1&&($status=="NOANSWER"||$status=="BUSY")){
						$agi->exec("Playback",PROMPTPATH.$voicemail_message);
						$agi->exec("Voicemail",$voicemail_extension."@voicemail-".$client_id.",s");
						$agi->hangup();
					}
					else{
						$agi->hangup();
					}
				}
				elseif($app=="MESSAGE"){$data = basename($data,".wav");$agi->exec("Playback",PROMPTPATH.$data);$agi->hangup();}
				elseif($app=="VOICEMAIL"){$agi->exec("Playback",PROMPTPATH.$voicemail_message);$agi->exec("Voicemail",$data."@voicemail-".$client_id.",s");$agi->hangup();}
				elseif($app=="QUEUE"){
					if($training_quality_enabled==1){$agi->exec("Playback",PROMPTPATH.$training_quality_message);}
					$agi->exec("Queue",$data.",Tt,,,".$timeout);
					
					$res = $agi->get_variable('QUEUESTATUS');
					$resultcode = $res['result'];
					$queue_status = $res['data'];
					if($queue_status=='TIMEOUT'&&$voicemail_enabled==1){
						$agi->exec("Playback",PROMPTPATH.$voicemail_message);
						$agi->exec("Voicemail",$voicemail_extension."@voicemail-".$client_id.",s");
						$agi->hangup();
					}
					else{
						$agi->hangup();
					}
					
				}
				elseif($app=="CONFERENCE"){
					if($training_quality_enabled==1){$agi->exec("Playback",PROMPTPATH.$training_quality_message);}
					$agi->exec("Confbridge",$data.",default_bridge,default_user,sample_user_menu");
					$agi->hangup();
				}
				elseif($app=="IVR"){
					$anotherivr = 1;
					$q = mysqli_query($con,"SELECT * FROM ivr_menus WHERE id=$data");
					$row = mysqli_fetch_array($q);
					
					$ivr_message = $row['ivr_message'];
					$ivr_message = PROMPTPATH.$ivr_message;$ivr_message = basename($ivr_message,".wav");
					$ivr = $row['ivr_json'];
					$ivr = json_decode($ivr,true);
					goto in_hours_ivr_to_ivr;
				}
				else{$agi->hangup();}
				
			}
			else{
				//invalid key press
				$agi->verbose("**** INVALID KEY OR TIMEOUT $ivrdata *******");
				if($ivrdata=="timeout"){
					if($timeoutcount>=3||$anothertimeoutcount>=3){$agi->hangup();}
					$agi->exec("Playback",PROMPTPATH.$timeout_message);
					if($anotherivr==0){$timeoutcount = $timeoutcount+1;goto ivr_again;}
					else{$anothertimeoutcount = $anothertimeoutcount+1;goto in_hours_ivr_to_ivr;}
				}
				else{
					if($invalidcount>=3||$anotherinvalidcount>=3){$agi->hangup();}
					$agi->exec("Playback",PROMPTPATH.$invalid_message);
					if($anotherivr==0){$invalidcount = $invalidcount+1;goto ivr_again;}
					else{$anotherinvalidcount = $anotherinvalidcount+1;goto in_hours_ivr_to_ivr;}
				}
				
			}
			
		}
		elseif($main_action=="QUEUE"){
			if($welcome_enabled==1){
				if($call_record==1){
					$agi->exec("Playback",PROMPTPATH.$welcome_message);
					if($training_quality_enabled==1){$agi->exec("Playback",PROMPTPATH.$training_quality_message);}
					$agi->exec("MixMonitor",$callid.".wav,ab");
					$agi->exec("Queue",$queue.",Tt,,,".$timeout);
					
					$res = $agi->get_variable('QUEUESTATUS');
					$resultcode = $res['result'];
					$queue_status = $res['data'];
					if($queue_status=='TIMEOUT'&&$voicemail_enabled==1){
						$agi->exec("Playback",PROMPTPATH.$voicemail_message);
						$agi->exec("Voicemail",$voicemail_extension."@voicemail-".$client_id.",s");
						$agi->hangup();
					}
					else{
						$agi->hangup();
					}
					
				}
				else{
					$agi->exec("Playback",PROMPTPATH.$welcome_message);
					if($training_quality_enabled==1){$agi->exec("Playback",PROMPTPATH.$training_quality_message);}
					$agi->exec("Queue",$queue.",Tt,,,".$timeout);
					
					$res = $agi->get_variable('QUEUESTATUS');
					$resultcode = $res['result'];
					$queue_status = $res['data'];
					if($queue_status=='TIMEOUT'&&$voicemail_enabled==1){
						$agi->exec("Playback",PROMPTPATH.$voicemail_message);
						$agi->exec("Voicemail",$voicemail_extension."@voicemail-".$client_id.",s");
						$agi->hangup();
					}
					else{
						$agi->hangup();
					}
					
				}
			}
			else{
				if($call_record==1){
					if($training_quality_enabled==1){$agi->exec("Playback",PROMPTPATH.$training_quality_message);}
					$agi->exec("MixMonitor",$callid.".wav,ab");
					$agi->exec("Queue",$queue.",Tt,,,".$timeout);
					
					$res = $agi->get_variable('QUEUESTATUS');
					$resultcode = $res['result'];
					$queue_status = $res['data'];
					if($queue_status=='TIMEOUT'&&$voicemail_enabled==1){
						$agi->exec("Playback",PROMPTPATH.$voicemail_message);
						$agi->exec("Voicemail",$voicemail_extension."@voicemail-".$client_id.",s");
						$agi->hangup();
					}
					else{
						$agi->hangup();
					}
					
				}
				else{
					if($training_quality_enabled==1){$agi->exec("Playback",PROMPTPATH.$training_quality_message);}
					$agi->exec("Queue",$queue.",Tt,,,".$timeout);
					
					$res = $agi->get_variable('QUEUESTATUS');
					$resultcode = $res['result'];
					$queue_status = $res['data'];
					if($queue_status=='TIMEOUT'&&$voicemail_enabled==1){
						$agi->exec("Playback",PROMPTPATH.$voicemail_message);
						$agi->exec("Voicemail",$voicemail_extension."@voicemail-".$client_id.",s");
						$agi->hangup();
					}
					else{
						$agi->hangup();
					}
					
				}
			}
		}
		elseif($main_action=="CONFERENCE"){
			if($welcome_enabled==1){
				if($call_record==1){
					$agi->exec("Playback",PROMPTPATH.$welcome_message);
					if($training_quality_enabled==1){$agi->exec("Playback",PROMPTPATH.$training_quality_message);}
					$agi->exec("MixMonitor",$callid.".wav,ab");
					$agi->exec("Confbridge",$conference.",default_bridge,default_user,sample_user_menu");
					$agi->hangup();
				}
				else{
					$agi->exec("Playback",PROMPTPATH.$welcome_message);
					if($training_quality_enabled==1){$agi->exec("Playback",PROMPTPATH.$training_quality_message);}
					$agi->exec("Confbridge",$conference.",default_bridge,default_user,sample_user_menu");
					$agi->hangup();
				}
			}
			else{
				if($call_record==1){
					if($training_quality_enabled==1){$agi->exec("Playback",PROMPTPATH.$training_quality_message);}
					$agi->exec("MixMonitor",$callid.".wav,ab");
					$agi->exec("Confbridge",$conference.",default_bridge,default_user,sample_user_menu");
					$agi->hangup();
				}
				else{
					if($training_quality_enabled==1){$agi->exec("Playback",PROMPTPATH.$training_quality_message);}
					$agi->exec("Confbridge",$conference.",default_bridge,default_user,sample_user_menu");
					$agi->hangup();
				}
			}
		}
			
		}
		else{
			//out of business hours
			$agi->verbose("**** OUT OF BUSINESS HOURS *******");
			if($after_hour_action=="EXTENSION"){
				if($welcome_enabled==1){
				if($call_record==1){
					$agi->exec("Playback",PROMPTPATH.$welcome_message);
					$agi->exec("MixMonitor",$callid.".wav,ab");
					if($training_quality_enabled==1){$agi->exec("Playback",PROMPTPATH.$training_quality_message);}
					if($moh_enabled==1){
						$agi->exec("Dial","PJSIP/".$after_extension.",".$timeout.",m(".$moh_name.")");
					}
					else{
						$agi->exec("Dial","PJSIP/".$after_extension.",".$timeout);
					}
					
					
					$res = $agi->get_variable('DIALSTATUS');
					$resultcode = $res['result'];
					$status = $res['data'];//dialstatus checking
					$agi->verbose("**** CALL STATUS , $status AND RESULT CODE $resultcode *******");
					if($after_voicemail_enabled==1&&($status=="NOANSWER"||$status=="BUSY")){
						$agi->exec("Playback",PROMPTPATH.$after_voicemail_message);
						$agi->exec("Voicemail",$after_voicemail_extension."@voicemail-".$client_id.",s");
						$agi->hangup();
					}
					else{
						$agi->hangup();
					}
				}
				else{
					$agi->exec("Playback",PROMPTPATH.$welcome_message);
					if($training_quality_enabled==1){$agi->exec("Playback",PROMPTPATH.$training_quality_message);}
					if($moh_enabled==1){
						$agi->exec("Dial","PJSIP/".$after_extension.",".$timeout.",m(".$moh_name.")");
					}
					else{
						$agi->exec("Dial","PJSIP/".$after_extension.",".$timeout);
					}
					
					$res = $agi->get_variable('DIALSTATUS');
					$resultcode = $res['result'];
					$status = $res['data'];//dialstatus checking
					$agi->verbose("**** CALL STATUS , $status AND RESULT CODE $resultcode *******");
					if($after_voicemail_enabled==1&&($status=="NOANSWER"||$status=="BUSY")){
						$agi->exec("Playback",PROMPTPATH.$after_voicemail_message);
						$agi->exec("Voicemail",$after_voicemail_extension."@voicemail-".$client_id.",s");
						$agi->hangup();
					}
					else{
						$agi->hangup();
					}
				}
			}
			else{
				if($call_record==1){
					$agi->exec("MixMonitor",$callid.".wav,ab");
					if($training_quality_enabled==1){$agi->exec("Playback",PROMPTPATH.$training_quality_message);}
					if($moh_enabled==1){
						$agi->exec("Dial","PJSIP/".$after_extension.",".$timeout.",m(".$moh_name.")");
					}
					else{
						$agi->exec("Dial","PJSIP/".$after_extension.",".$timeout);
					}
					
					$res = $agi->get_variable('DIALSTATUS');
					$resultcode = $res['result'];
					$status = $res['data'];//dialstatus checking
					$agi->verbose("**** CALL STATUS , $status AND RESULT CODE $resultcode *******");
					if($after_voicemail_enabled==1&&($status=="NOANSWER"||$status=="BUSY")){
						$agi->exec("Playback",PROMPTPATH.$after_voicemail_message);
						$agi->exec("Voicemail",$after_voicemail_extension."@voicemail-".$client_id.",s");
						$agi->hangup();
					}
					else{
						$agi->hangup();
					}
				}
				else{
					if($training_quality_enabled==1){$agi->exec("Playback",PROMPTPATH.$training_quality_message);}
					if($moh_enabled==1){
						$agi->exec("Dial","PJSIP/".$after_extension.",".$timeout.",m(".$moh_name.")");
					}
					else{
						$agi->exec("Dial","PJSIP/".$after_extension.",".$timeout);
					}
					
					$res = $agi->get_variable('DIALSTATUS');
					$resultcode = $res['result'];
					$status = $res['data'];//dialstatus checking
					$agi->verbose("**** CALL STATUS , $status AND RESULT CODE $resultcode *******");
					if($after_voicemail_enabled==1&&($status=="NOANSWER"||$status=="BUSY")){
						$agi->exec("Playback",PROMPTPATH.$after_voicemail_message);
						$agi->exec("Voicemail",$after_voicemail_extension."@voicemail-".$client_id.",s");
						$agi->hangup();
					}
					else{
						$agi->hangup();
					}
				}
				
			}
			}
			elseif($after_hour_action=="DEPARTMENT"){
				$qafterdept = mysqli_query($con,"SELECT * FROM st_department WHERE id=$after_department");
				$rowafterdept = mysqli_fetch_array($qafterdept);
				$after_department = $rowafterdept['json_string'];
				if($welcome_enabled==1){
				if($call_record==1){
					$agi->exec("Playback",PROMPTPATH.$welcome_message);
					$agi->exec("MixMonitor",$callid.".wav,ab");
					$agi->exec("Set","CALLERID(num)=".$callee);
					if($training_quality_enabled==1){$agi->exec("Playback",PROMPTPATH.$training_quality_message);}
					if($moh_enabled==1){
						$agi->exec("Dial",$after_department.",".$timeout.",m(".$moh_name.")");
					}
					else{
						$agi->exec("Dial",$after_department.",".$timeout);
					}
					
					
					$res = $agi->get_variable('DIALSTATUS');
					$resultcode = $res['result'];
					$status = $res['data'];//dialstatus checking
					$agi->verbose("**** CALL STATUS , $status AND RESULT CODE $resultcode *******");
					if($after_voicemail_enabled==1&&($status=="NOANSWER"||$status=="BUSY")){
						$agi->exec("Playback",PROMPTPATH.$after_voicemail_message);
						$agi->exec("Voicemail",$after_voicemail_extension."@voicemail-".$client_id.",s");
						$agi->hangup();
					}
					else{
						$agi->hangup();
					}
				}
				else{
					$agi->exec("Playback",PROMPTPATH.$welcome_message);
					$agi->exec("Set","CALLERID(num)=".$callee);
					if($training_quality_enabled==1){$agi->exec("Playback",PROMPTPATH.$training_quality_message);}
					if($moh_enabled==1){
						$agi->exec("Dial",$after_department.",".$timeout.",m(".$moh_name.")");
					}
					else{
						$agi->exec("Dial",$after_department.",".$timeout);
					}
					
					$res = $agi->get_variable('DIALSTATUS');
					$resultcode = $res['result'];
					$status = $res['data'];//dialstatus checking
					$agi->verbose("**** CALL STATUS , $status AND RESULT CODE $resultcode *******");
					if($after_voicemail_enabled==1&&($status=="NOANSWER"||$status=="BUSY")){
						$agi->exec("Playback",PROMPTPATH.$after_voicemail_message);
						$agi->exec("Voicemail",$after_voicemail_extension."@voicemail-".$client_id.",s");
						$agi->hangup();
					}
					else{
						$agi->hangup();
					}
				}
			}
			else{
				if($call_record==1){
					$agi->exec("MixMonitor",$callid.".wav,ab");
					$agi->exec("Set","CALLERID(num)=".$callee);
					if($training_quality_enabled==1){$agi->exec("Playback",PROMPTPATH.$training_quality_message);}
					if($moh_enabled==1){
						$agi->exec("Dial",$after_department.",".$timeout.",m(".$moh_name.")");
					}
					else{
						$agi->exec("Dial",$after_department.",".$timeout);
					}
					
					$res = $agi->get_variable('DIALSTATUS');
					$resultcode = $res['result'];
					$status = $res['data'];//dialstatus checking
					$agi->verbose("**** CALL STATUS , $status AND RESULT CODE $resultcode *******");
					if($after_voicemail_enabled==1&&($status=="NOANSWER"||$status=="BUSY")){
						$agi->exec("Playback",PROMPTPATH.$after_voicemail_message);
						$agi->exec("Voicemail",$after_voicemail_extension."@voicemail-".$client_id.",s");
						$agi->hangup();
					}
					else{
						$agi->hangup();
					}
				}
				else{
					$agi->exec("Set","CALLERID(num)=".$callee);
					if($training_quality_enabled==1){$agi->exec("Playback",PROMPTPATH.$training_quality_message);}
					if($moh_enabled==1){
						$agi->exec("Dial",$after_department.",".$timeout.",m(".$moh_name.")");
					}
					else{
						$agi->exec("Dial",$after_department.",".$timeout);
					}
					
					$res = $agi->get_variable('DIALSTATUS');
					$resultcode = $res['result'];
					$status = $res['data'];//dialstatus checking
					$agi->verbose("**** CALL STATUS , $status AND RESULT CODE $resultcode *******");
					if($after_voicemail_enabled==1&&($status=="NOANSWER"||$status=="BUSY")){
						$agi->exec("Playback",PROMPTPATH.$after_voicemail_message);
						$agi->exec("Voicemail",$after_voicemail_extension."@voicemail-".$client_id.",s");
						$agi->hangup();
					}
					else{
						$agi->hangup();
					}
				}
			}
			}
			elseif($after_hour_action=="PHONE"){
				if($welcome_enabled==1){
				if($call_record==1){
					$agi->exec("Playback",PROMPTPATH.$welcome_message);
					$agi->exec("MixMonitor",$callid.".wav,ab");
					$agi->exec("Set","CALLERID(num)=".$callee);
					if($training_quality_enabled==1){$agi->exec("Playback",PROMPTPATH.$training_quality_message);}
					if($moh_enabled==1){
						$agi->exec("Dial","PJSIP/".$after_phone."@mtntrunk,".$timeout.",m(".$moh_name.")");
					}
					else{
						$agi->exec("Dial","PJSIP/".$after_phone."@mtntrunk,".$timeout);
					}
					
					
					$res = $agi->get_variable('DIALSTATUS');
					$resultcode = $res['result'];
					$status = $res['data'];//dialstatus checking
					$agi->verbose("**** CALL STATUS , $status AND RESULT CODE $resultcode *******");
					if($after_voicemail_enabled==1&&($status=="NOANSWER"||$status=="BUSY")){
						$agi->exec("Playback",PROMPTPATH.$after_voicemail_message);
						$agi->exec("Voicemail",$after_voicemail_extension."@voicemail-".$client_id.",s");
						$agi->hangup();
					}
					else{
						$agi->hangup();
					}
				}
				else{
					$agi->exec("Playback",PROMPTPATH.$welcome_message);
					$agi->exec("Set","CALLERID(num)=".$callee);
					if($training_quality_enabled==1){$agi->exec("Playback",PROMPTPATH.$training_quality_message);}
					if($moh_enabled==1){
						$agi->exec("Dial","PJSIP/".$after_phone."@mtntrunk,".$timeout.",m(".$moh_name.")");
					}
					else{
						$agi->exec("Dial","PJSIP/".$after_phone."@mtntrunk,".$timeout);
					}
					
					$res = $agi->get_variable('DIALSTATUS');
					$resultcode = $res['result'];
					$status = $res['data'];//dialstatus checking
					$agi->verbose("**** CALL STATUS , $status AND RESULT CODE $resultcode *******");
					if($after_voicemail_enabled==1&&($status=="NOANSWER"||$status=="BUSY")){
						$agi->exec("Playback",PROMPTPATH.$after_voicemail_message);
						$agi->exec("Voicemail",$after_voicemail_extension."@voicemail-".$client_id.",s");
						$agi->hangup();
					}
					else{
						$agi->hangup();
					}
				}
			}
			else{
				if($call_record==1){
					$agi->exec("MixMonitor",$callid.".wav,ab");
					$agi->exec("Set","CALLERID(num)=".$callee);
					if($training_quality_enabled==1){$agi->exec("Playback",PROMPTPATH.$training_quality_message);}
					if($moh_enabled==1){
						$agi->exec("Dial","PJSIP/".$after_phone."@mtntrunk,".$timeout.",m(".$moh_name.")");
					}
					else{
						$agi->exec("Dial","PJSIP/".$after_phone."@mtntrunk,".$timeout);
					}
					
					$res = $agi->get_variable('DIALSTATUS');
					$resultcode = $res['result'];
					$status = $res['data'];//dialstatus checking
					$agi->verbose("**** CALL STATUS , $status AND RESULT CODE $resultcode *******");
					if($after_voicemail_enabled==1&&($status=="NOANSWER"||$status=="BUSY")){
						$agi->exec("Playback",PROMPTPATH.$after_voicemail_message);
						$agi->exec("Voicemail",$after_voicemail_extension."@voicemail-".$client_id.",s");
						$agi->hangup();
					}
					else{
						$agi->hangup();
					}
				}
				else{
					$agi->exec("Set","CALLERID(num)=".$callee);
					if($training_quality_enabled==1){$agi->exec("Playback",PROMPTPATH.$training_quality_message);}
					if($moh_enabled==1){
						$agi->exec("Dial","PJSIP/".$after_phone."@mtntrunk,".$timeout.",m(".$moh_name.")");
					}
					else{
						$agi->exec("Dial","PJSIP/".$after_phone."@mtntrunk,".$timeout);
					}
					
					$res = $agi->get_variable('DIALSTATUS');
					$resultcode = $res['result'];
					$status = $res['data'];//dialstatus checking
					$agi->verbose("**** CALL STATUS , $status AND RESULT CODE $resultcode *******");
					if($after_voicemail_enabled==1&&($status=="NOANSWER"||$status=="BUSY")){
						$agi->exec("Playback",PROMPTPATH.$after_voicemail_message);
						$agi->exec("Voicemail",$after_voicemail_extension."@voicemail-".$client_id.",s");
						$agi->hangup();
					}
					else{
						$agi->hangup();
					}
				}
			}
			}
			elseif($after_hour_action=="MESSAGE"){
				if($welcome_enabled==1){
					$agi->exec("Playback",PROMPTPATH.$welcome_message);
					$agi->exec("Playback",PROMPTPATH.$after_message);
				}
				else{
					$agi->exec("Playback",PROMPTPATH.$after_message);
				}
			}
			elseif($after_hour_action=="VOICEMAIL"){
				if($welcome_enabled==1){
					$agi->exec("Playback",PROMPTPATH.$welcome_message);
					$agi->exec("Playback",PROMPTPATH.$after_voicemail_message);
					$agi->exec("Voicemail",$after_voicemail_extension."@voicemail-".$client_id.",s");
					$agi->hangup();
				}
				else{
					$agi->exec("Playback",PROMPTPATH.$after_voicemail_message);
					$agi->exec("Voicemail",$after_voicemail_extension."@voicemail-".$client_id.",s");
					$agi->hangup();
				}
			}
			elseif($after_hour_action=="DIRECTORY"){
				
			}
			elseif($after_hour_action=="IVR"){
				$anotherivr = 0;
				$timeoutcount = 0;$invalidcount = 0;
				$anothertimeoutcount = 0;$anotherinvalidcount = 0;
				$after_ivr = json_decode($after_ivr,true);//after ivr can be placed
				after_ivr_again:
				if($welcome_enabled==1){$agi->exec("Playback",PROMPTPATH.$welcome_message);}
				if($call_record==1){$agi->exec("MixMonitor",$callid.".wav,ab");}
			
				out_hours_ivr_to_ivr:
				$result = $agi->get_data(PROMPTPATH.$after_ivr_message, 5000, 1);
				$digit = $result['result'];
				$ivrdata = $result['data'];
				$press_key = "press".$digit;
			
				if(array_key_exists($press_key,$after_ivr)){
					$agi->verbose("**** $press_key *******");
					$app = $after_ivr[$press_key]['App'];
					$data = $after_ivr[$press_key]['Data'];
				
					if($app=="EXTENSION"){
						if($training_quality_enabled==1){$agi->exec("Playback",PROMPTPATH.$training_quality_message);}
						if($moh_enabled==1){
							$agi->exec("Dial","PJSIP/".$data.",".$timeout.",m(".$moh_name.")");
						}
						else{
							$agi->exec("Dial","PJSIP/".$data.",".$timeout);
						}
						
						
						$res = $agi->get_variable('DIALSTATUS');
						$resultcode = $res['result'];
						$status = $res['data'];//dialstatus checking
						$agi->verbose("**** CALL STATUS , $status AND RESULT CODE $resultcode *******");
						if($after_voicemail_enabled==1&&($status=="NOANSWER"||$status=="BUSY")){
							$agi->exec("Playback",PROMPTPATH.$after_voicemail_message);
							$agi->exec("Voicemail",$after_voicemail_extension."@voicemail-".$client_id.",s");
							$agi->hangup();
						}
						else{
							$agi->hangup();
						}
					}
					elseif($app=="DEPARTMENT"){
						$qafterdept = mysqli_query($con,"SELECT * FROM st_department WHERE id=$data");
						$rowafterdept = mysqli_fetch_array($qafterdept);
						$data = $rowafterdept['json_string'];
						$agi->exec("Set","CALLERID(num)=".$callee);
						if($training_quality_enabled==1){$agi->exec("Playback",PROMPTPATH.$training_quality_message);}
						if($moh_enabled==1){
							$agi->exec("Dial",$data.",".$timeout.",m(".$moh_name.")");
						}
						else{
							$agi->exec("Dial",$data.",".$timeout);
						}
						
						
						$res = $agi->get_variable('DIALSTATUS');
						$resultcode = $res['result'];
						$status = $res['data'];//dialstatus checking
						$agi->verbose("**** CALL STATUS , $status AND RESULT CODE $resultcode *******");
						if($after_voicemail_enabled==1&&($status=="NOANSWER"||$status=="BUSY")){
							$agi->exec("Playback",PROMPTPATH.$after_voicemail_message);
							$agi->exec("Voicemail",$after_voicemail_extension."@voicemail-".$client_id.",s");
							$agi->hangup();
						}
						else{
							$agi->hangup();
						}
					}
					elseif($app=="PHONE"){
						$agi->exec("Set","CALLERID(num)=".$callee);
						if($training_quality_enabled==1){$agi->exec("Playback",PROMPTPATH.$training_quality_message);}
						if($moh_enabled==1){
							$agi->exec("Dial","PJSIP/".$data."@mtntrunk,",$timeout.",m(".$moh_name.")");
						}
						else{
							$agi->exec("Dial","PJSIP/".$data."@mtntrunk,",$timeout);
						}
						
						
						$res = $agi->get_variable('DIALSTATUS');
						$resultcode = $res['result'];
						$status = $res['data'];//dialstatus checking
						$agi->verbose("**** CALL STATUS , $status AND RESULT CODE $resultcode *******");
						if($after_voicemail_enabled==1&&($status=="NOANSWER"||$status=="BUSY")){
							$agi->exec("Playback",PROMPTPATH.$after_voicemail_message);
							$agi->exec("Voicemail",$after_voicemail_extension."@voicemail-".$client_id.",s");
							$agi->hangup();
						}
						else{
							$agi->hangup();
						}
					}
					elseif($app=="MESSAGE"){$data = basename($data,".wav");$agi->exec("Playback",PROMPTPATH.$data);$agi->hangup();}
					elseif($app=="VOICEMAIL"){$agi->exec("Playback",PROMPTPATH.$after_voicemail_message);$agi->exec("Voicemail",$data."@voicemail-".$client_id.",s");$agi->hangup();}
					elseif($app=="QUEUE"){
						if($training_quality_enabled==1){$agi->exec("Playback",PROMPTPATH.$training_quality_message);}
						$agi->exec("Queue",$data.",Tt,,,".$timeout);
						
						$res = $agi->get_variable('QUEUESTATUS');
						$resultcode = $res['result'];
						$queue_status = $res['data'];
						if($queue_status=='TIMEOUT'&&$after_voicemail_enabled==1){
							$agi->exec("Playback",PROMPTPATH.$after_voicemail_message);
							$agi->exec("Voicemail",$after_voicemail_extension."@voicemail-".$client_id.",s");
							$agi->hangup();
						}
						else{
							$agi->hangup();
						}
						
					}
					elseif($app=="CONFERENCE"){
						if($training_quality_enabled==1){$agi->exec("Playback",PROMPTPATH.$training_quality_message);}
						$agi->exec("Confbridge",$data.",default_bridge,default_user,sample_user_menu");
						$agi->hangup();
					}
					elseif($app=="IVR"){
						$anotherivr = 1;
					$q = mysqli_query($con,"SELECT * FROM ivr_menus WHERE id=$data");
					$row = mysqli_fetch_array($q);
					
					$ivr_message = $row['ivr_message'];
					$ivr_message = PROMPTPATH.$ivr_message;$ivr_message = basename($ivr_message,".wav");
					$ivr = $row['ivr_json'];
					$ivr = json_decode($ivr,true);
					goto out_hours_ivr_to_ivr;
				}
					else{$agi->hangup();}
				
				}
				else{
					//invalid key press or timeout
					$agi->verbose("**** INVALID KEY OR TIMEOUT *******");
					if($ivrdata=="timeout"){
						if($timeoutcount>=3||$anothertimeoutcount>=3){$agi->hangup();}
						$agi->exec("Playback",PROMPTPATH.$timeout_message);
						if($anotherivr==0){$timeoutcount = $timeoutcount+1;goto after_ivr_again;}
						else{$anothertimeoutcount = $anothertimeoutcount+1;goto out_hours_ivr_to_ivr;}
					}
					else{
						if($invalidcount>=3||$anotherinvalidcount>=3){$agi->hangup();}
						$agi->exec("Playback",PROMPTPATH.$invalid_message);
						if($anotherivr==0){$invalidcount = $invalidcount+1;goto after_ivr_again;}
						else{$anotherinvalidcount = $anotherinvalidcount+1;goto out_hours_ivr_to_ivr;}
					}
					
				}
			}
			elseif($after_hour_action=="QUEUE"){
			if($welcome_enabled==1){
				if($call_record==1){
					$agi->exec("Playback",PROMPTPATH.$welcome_message);
					if($training_quality_enabled==1){$agi->exec("Playback",PROMPTPATH.$training_quality_message);}
					$agi->exec("MixMonitor",$callid.".wav,ab");
					$agi->exec("Queue",$after_queue.",Tt,,,".$timeout);
					
					$res = $agi->get_variable('QUEUESTATUS');
					$resultcode = $res['result'];
					$queue_status = $res['data'];
					if($queue_status=='TIMEOUT'&&$after_voicemail_enabled==1){
						$agi->exec("Playback",PROMPTPATH.$after_voicemail_message);
						$agi->exec("Voicemail",$after_voicemail_extension."@voicemail-".$client_id.",s");
						$agi->hangup();
					}
					else{
						$agi->hangup();
					}
					
				}
				else{
					$agi->exec("Playback",PROMPTPATH.$welcome_message);
					if($training_quality_enabled==1){$agi->exec("Playback",PROMPTPATH.$training_quality_message);}
					$agi->exec("Queue",$after_queue.",Tt,,,".$timeout);
					
					$res = $agi->get_variable('QUEUESTATUS');
					$resultcode = $res['result'];
					$queue_status = $res['data'];
					if($queue_status=='TIMEOUT'&&$after_voicemail_enabled==1){
						$agi->exec("Playback",PROMPTPATH.$after_voicemail_message);
						$agi->exec("Voicemail",$after_voicemail_extension."@voicemail-".$client_id.",s");
						$agi->hangup();
					}
					else{
						$agi->hangup();
					}
					
				}
			}
			else{
				if($call_record==1){
					if($training_quality_enabled==1){$agi->exec("Playback",PROMPTPATH.$training_quality_message);}
					$agi->exec("MixMonitor",$callid.".wav,ab");
					$agi->exec("Queue",$after_queue.",Tt,,,".$timeout);
					
					$res = $agi->get_variable('QUEUESTATUS');
					$resultcode = $res['result'];
					$queue_status = $res['data'];
					if($queue_status=='TIMEOUT'&&$after_voicemail_enabled==1){
						$agi->exec("Playback",PROMPTPATH.$after_voicemail_message);
						$agi->exec("Voicemail",$after_voicemail_extension."@voicemail-".$client_id.",s");
						$agi->hangup();
					}
					else{
						$agi->hangup();
					}
					
				}
				else{
					if($training_quality_enabled==1){$agi->exec("Playback",PROMPTPATH.$training_quality_message);}
					$agi->exec("Queue",$after_queue.",Tt,,,".$timeout);
					
					$res = $agi->get_variable('QUEUESTATUS');
					$resultcode = $res['result'];
					$queue_status = $res['data'];
					if($queue_status=='TIMEOUT'&&$after_voicemail_enabled==1){
						$agi->exec("Playback",PROMPTPATH.$after_voicemail_message);
						$agi->exec("Voicemail",$after_voicemail_extension."@voicemail-".$client_id.",s");
						$agi->hangup();
					}
					else{
						$agi->hangup();
					}
					
				}
			}
		}
		elseif($main_action=="CONFERENCE"){
			if($welcome_enabled==1){
				if($call_record==1){
					$agi->exec("Playback",PROMPTPATH.$welcome_message);
					if($training_quality_enabled==1){$agi->exec("Playback",PROMPTPATH.$training_quality_message);}
					$agi->exec("MixMonitor",$callid.".wav,ab");
					$agi->exec("Confbridge",$after_conference.",default_bridge,default_user,sample_user_menu");
					$agi->hangup();
				}
				else{
					$agi->exec("Playback",PROMPTPATH.$welcome_message);
					if($training_quality_enabled==1){$agi->exec("Playback",PROMPTPATH.$training_quality_message);}
					$agi->exec("Confbridge",$after_conference.",default_bridge,default_user,sample_user_menu");
					$agi->hangup();
				}
			}
			else{
				if($call_record==1){
					if($training_quality_enabled==1){$agi->exec("Playback",PROMPTPATH.$training_quality_message);}
					$agi->exec("MixMonitor",$callid.".wav,ab");
					$agi->exec("Confbridge",$after_conference.",default_bridge,default_user,sample_user_menu");
					$agi->hangup();
				}
				else{
					if($training_quality_enabled==1){$agi->exec("Playback",PROMPTPATH.$training_quality_message);}
					$agi->exec("Confbridge",$after_conference.",default_bridge,default_user,sample_user_menu");
					$agi->hangup();
				}
			}
		}
		}
	}
		
	
	//always open case
	else{
		if($main_action=="EXTENSION"){
			if($welcome_enabled==1){
				if($call_record==1){
					$agi->exec("Playback",PROMPTPATH.$welcome_message);
					$agi->exec("MixMonitor",$callid.".wav,ab");
					if($training_quality_enabled==1){$agi->exec("Playback",PROMPTPATH.$training_quality_message);}
					if($moh_enabled==1){
						$agi->exec("Dial","PJSIP/".$extension.",".$timeout.",m(".$moh_name.")");
					}
					else{
						$agi->exec("Dial","PJSIP/".$extension.",".$timeout);
					}
					
					
					$res = $agi->get_variable('DIALSTATUS');
					$resultcode = $res['result'];
					$status = $res['data'];//dialstatus checking
					$agi->verbose("**** CALL STATUS , $status AND RESULT CODE $resultcode *******");
					if($voicemail_enabled==1&&($status=="NOANSWER"||$status=="BUSY")){
						$agi->exec("Playback",PROMPTPATH.$voicemail_message);
						$agi->exec("Voicemail",$voicemail_extension."@voicemail-".$client_id.",s");
						$agi->hangup();
					}
					else{
						$agi->hangup();
					}
					
				}
				else{
					$agi->exec("Playback",PROMPTPATH.$welcome_message);
					if($training_quality_enabled==1){$agi->exec("Playback",PROMPTPATH.$training_quality_message);}
					if($moh_enabled==1){
						$agi->exec("Dial","PJSIP/".$extension.",".$timeout.",m(".$moh_name.")");
					}
					else{
						$agi->exec("Dial","PJSIP/".$extension.",".$timeout);
					}
					
					$res = $agi->get_variable('DIALSTATUS');
					$resultcode = $res['result'];
					$status = $res['data'];//dialstatus checking
					$agi->verbose("**** CALL STATUS , $status AND RESULT CODE $resultcode *******");
					if($voicemail_enabled==1&&($status=="NOANSWER"||$status=="BUSY")){
						$agi->exec("Playback",PROMPTPATH.$voicemail_message);
						$agi->exec("Voicemail",$voicemail_extension."@voicemail-".$client_id.",s");
						$agi->hangup();
					}
					else{
						$agi->hangup();
					}
				}
			}
			else{
				if($call_record==1){
					$agi->exec("MixMonitor",$callid.".wav,ab");
					if($training_quality_enabled==1){$agi->exec("Playback",PROMPTPATH.$training_quality_message);}
					if($moh_enabled==1){
						$agi->exec("Dial","PJSIP/".$extension.",".$timeout.",m(".$moh_name.")");
					}
					else{
						$agi->exec("Dial","PJSIP/".$extension.",".$timeout);
					}
					
					$res = $agi->get_variable('DIALSTATUS');
					$resultcode = $res['result'];
					$status = $res['data'];//dialstatus checking
					$agi->verbose("**** CALL STATUS , $status AND RESULT CODE $resultcode *******");
					if($voicemail_enabled==1&&($status=="NOANSWER"||$status=="BUSY")){
						$agi->exec("Playback",PROMPTPATH.$voicemail_message);
						$agi->exec("Voicemail",$voicemail_extension."@voicemail-".$client_id.",s");
						$agi->hangup();
					}
					else{
						$agi->hangup();
					}
				}
				else{
					if($training_quality_enabled==1){$agi->exec("Playback",PROMPTPATH.$training_quality_message);}
					if($moh_enabled==1){
						$agi->exec("Dial","PJSIP/".$extension.",".$timeout.",m(".$moh_name.")");
					}
					else{
						$agi->exec("Dial","PJSIP/".$extension.",".$timeout);
					}
					
					$res = $agi->get_variable('DIALSTATUS');
					$resultcode = $res['result'];
					$status = $res['data'];//dialstatus checking
					$agi->verbose("**** CALL STATUS , $status AND RESULT CODE $resultcode *******");
					if($voicemail_enabled==1&&($status=="NOANSWER"||$status=="BUSY")){
						$agi->exec("Playback",PROMPTPATH.$voicemail_message);
						$agi->exec("Voicemail",$voicemail_extension."@voicemail-".$client_id.",s");
						$agi->hangup();
					}
					else{
						$agi->hangup();
					}
				}
				
			}
			
		}
		elseif($main_action=="DEPARTMENT"){
			$qdept = mysqli_query($con,"SELECT * FROM st_department WHERE id=$department");
			$rowdept = mysqli_fetch_array($qdept);
			$department = $rowdept['json_string'];
			if($welcome_enabled==1){
				if($call_record==1){
					$agi->exec("Playback",PROMPTPATH.$welcome_message);
					$agi->exec("MixMonitor",$callid.".wav,ab");
					$agi->exec("Set","CALLERID(num)=".$callee);
					if($training_quality_enabled==1){$agi->exec("Playback",PROMPTPATH.$training_quality_message);}
					if($moh_enabled==1){
						$agi->exec("Dial",$department.",".$timeout.",m(".$moh_name.")");
					}
					else{
						$agi->exec("Dial",$department.",".$timeout);
					}
					
					
					$res = $agi->get_variable('DIALSTATUS');
					$resultcode = $res['result'];
					$status = $res['data'];//dialstatus checking
					$agi->verbose("**** CALL STATUS , $status AND RESULT CODE $resultcode *******");
					if($voicemail_enabled==1&&($status=="NOANSWER"||$status=="BUSY")){
						$agi->exec("Playback",PROMPTPATH.$voicemail_message);
						$agi->exec("Voicemail",$voicemail_extension."@voicemail-".$client_id.",s");
						$agi->hangup();
					}
					else{
						$agi->hangup();
					}
				}
				else{
					$agi->exec("Playback",PROMPTPATH.$welcome_message);
					$agi->exec("Set","CALLERID(num)=".$callee);
					if($training_quality_enabled==1){$agi->exec("Playback",PROMPTPATH.$training_quality_message);}
					if($moh_enabled==1){
						$agi->exec("Dial",$department.",".$timeout.",m(".$moh_name.")");
					}
					else{
						$agi->exec("Dial",$department.",".$timeout);
					}
					
					$res = $agi->get_variable('DIALSTATUS');
					$resultcode = $res['result'];
					$status = $res['data'];//dialstatus checking
					$agi->verbose("**** CALL STATUS , $status AND RESULT CODE $resultcode *******");
					if($voicemail_enabled==1&&($status=="NOANSWER"||$status=="BUSY")){
						$agi->exec("Playback",PROMPTPATH.$voicemail_message);
						$agi->exec("Voicemail",$voicemail_extension."@voicemail-".$client_id.",s");
						$agi->hangup();
					}
					else{
						$agi->hangup();
					}
				}
			}
			else{
				if($call_record==1){
					$agi->exec("MixMonitor",$callid.".wav,ab");
					$agi->exec("Set","CALLERID(num)=".$callee);
					if($training_quality_enabled==1){$agi->exec("Playback",PROMPTPATH.$training_quality_message);}
					if($moh_enabled==1){
						$agi->exec("Dial",$department.",".$timeout.",m(".$moh_name.")");
					}
					else{
						$agi->exec("Dial",$department.",".$timeout);
					}
					
					$res = $agi->get_variable('DIALSTATUS');
					$resultcode = $res['result'];
					$status = $res['data'];//dialstatus checking
					$agi->verbose("**** CALL STATUS , $status AND RESULT CODE $resultcode *******");
					if($voicemail_enabled==1&&($status=="NOANSWER"||$status=="BUSY")){
						$agi->exec("Playback",PROMPTPATH.$voicemail_message);
						$agi->exec("Voicemail",$voicemail_extension."@voicemail-".$client_id.",s");
						$agi->hangup();
					}
					else{
						$agi->hangup();
					}
				}
				else{
					$agi->exec("Set","CALLERID(num)=".$callee);
					if($training_quality_enabled==1){$agi->exec("Playback",PROMPTPATH.$training_quality_message);}
					if($moh_enabled==1){
						$agi->exec("Dial",$department.",".$timeout.",m(".$moh_name.")");
					}
					else{
						$agi->exec("Dial",$department.",".$timeout);
					}
					
					$res = $agi->get_variable('DIALSTATUS');
					$resultcode = $res['result'];
					$status = $res['data'];//dialstatus checking
					$agi->verbose("**** CALL STATUS , $status AND RESULT CODE $resultcode *******");
					if($voicemail_enabled==1&&($status=="NOANSWER"||$status=="BUSY")){
						$agi->exec("Playback",PROMPTPATH.$voicemail_message);
						$agi->exec("Voicemail",$voicemail_extension."@voicemail-".$client_id.",s");
						$agi->hangup();
					}
					else{
						$agi->hangup();
					}
				}
			}
		}
		elseif($main_action=="PHONE"){
			if($welcome_enabled==1){
				if($call_record==1){
					$agi->exec("Playback",PROMPTPATH.$welcome_message);
					$agi->exec("MixMonitor",$callid.".wav,ab");
					$agi->exec("Set","CALLERID(num)=".$callee);
					if($training_quality_enabled==1){$agi->exec("Playback",PROMPTPATH.$training_quality_message);}
					if($moh_enabled==1){
						$agi->exec("Dial","PJSIP/".$phone."@mtntrunk,".$timeout.",m(".$moh_name.")");
					}
					else{
						$agi->exec("Dial","PJSIP/".$phone."@mtntrunk,".$timeout);
					}
					
					
					$res = $agi->get_variable('DIALSTATUS');
					$resultcode = $res['result'];
					$status = $res['data'];//dialstatus checking
					$agi->verbose("**** CALL STATUS , $status AND RESULT CODE $resultcode *******");
					if($voicemail_enabled==1&&($status=="NOANSWER"||$status=="BUSY")){
						$agi->exec("Playback",PROMPTPATH.$voicemail_message);
						$agi->exec("Voicemail",$voicemail_extension."@voicemail-".$client_id.",s");
						$agi->hangup();
					}
					else{
						$agi->hangup();
					}
				}
				else{
					$agi->exec("Playback",PROMPTPATH.$welcome_message);
					$agi->exec("Set","CALLERID(num)=".$callee);
					if($training_quality_enabled==1){$agi->exec("Playback",PROMPTPATH.$training_quality_message);}
					if($moh_enabled==1){
						$agi->exec("Dial","PJSIP/".$phone."@mtntrunk,".$timeout.",m(".$moh_name.")");
					}
					else{
						$agi->exec("Dial","PJSIP/".$phone."@mtntrunk,".$timeout);
					}
					
					$res = $agi->get_variable('DIALSTATUS');
					$resultcode = $res['result'];
					$status = $res['data'];//dialstatus checking
					$agi->verbose("**** CALL STATUS , $status AND RESULT CODE $resultcode *******");
					if($voicemail_enabled==1&&($status=="NOANSWER"||$status=="BUSY")){
						$agi->exec("Playback",PROMPTPATH.$voicemail_message);
						$agi->exec("Voicemail",$voicemail_extension."@voicemail-".$client_id.",s");
						$agi->hangup();
					}
					else{
						$agi->hangup();
					}
				}
			}
			else{
				if($call_record==1){
					$agi->exec("MixMonitor",$callid.".wav,ab");
					$agi->exec("Set","CALLERID(num)=".$callee);
					if($training_quality_enabled==1){$agi->exec("Playback",PROMPTPATH.$training_quality_message);}
					if($moh_enabled==1){
						$agi->exec("Dial","PJSIP/".$phone."@mtntrunk,".$timeout.",m(".$moh_name.")");
					}
					else{
						$agi->exec("Dial","PJSIP/".$phone."@mtntrunk,".$timeout);
					}
					
					$res = $agi->get_variable('DIALSTATUS');
					$resultcode = $res['result'];
					$status = $res['data'];//dialstatus checking
					$agi->verbose("**** CALL STATUS , $status AND RESULT CODE $resultcode *******");
					if($voicemail_enabled==1&&($status=="NOANSWER"||$status=="BUSY")){
						$agi->exec("Playback",PROMPTPATH.$voicemail_message);
						$agi->exec("Voicemail",$voicemail_extension."@voicemail-".$client_id.",s");
						$agi->hangup();
					}
					else{
						$agi->hangup();
					}
				}
				else{
					$agi->exec("Set","CALLERID(num)=".$callee);
					if($training_quality_enabled==1){$agi->exec("Playback",PROMPTPATH.$training_quality_message);}
					if($moh_enabled==1){
						$agi->exec("Dial","PJSIP/".$phone."@mtntrunk,".$timeout.",m(".$moh_name.")");
					}
					else{
						$agi->exec("Dial","PJSIP/".$phone."@mtntrunk,".$timeout);
					}
					
					$res = $agi->get_variable('DIALSTATUS');
					$resultcode = $res['result'];
					$status = $res['data'];//dialstatus checking
					$agi->verbose("**** CALL STATUS , $status AND RESULT CODE $resultcode *******");
					if($voicemail_enabled==1&&($status=="NOANSWER"||$status=="BUSY")){
						$agi->exec("Playback",PROMPTPATH.$voicemail_message);
						$agi->exec("Voicemail",$voicemail_extension."@voicemail-".$client_id.",s");
						$agi->hangup();
					}
					else{
						$agi->hangup();
					}
				}
			}
		}
		elseif($main_action=="MESSAGE"){
			if($welcome_enabled==1){
				$agi->exec("Playback",PROMPTPATH.$welcome_message);
				$agi->exec("Playback",PROMPTPATH.$message);
			}
			else{
				$agi->exec("Playback",PROMPTPATH.$message);
			}
		}
		elseif($main_action=="VOICEMAIL"){
			if($welcome_enabled==1){
				$agi->exec("Playback",PROMPTPATH.$welcome_message);
				$agi->exec("Playback",PROMPTPATH.$voicemail_message);
				$agi->exec("Voicemail",$voicemail_extension."@voicemail-".$client_id.",s");
				$agi->hangup();
			}
			else{
				$agi->exec("Playback",PROMPTPATH.$voicemail_message);
				$agi->exec("Voicemail",$voicemail_extension."@voicemail-".$client_id.",s");
				$agi->hangup();
			}
		}
		elseif($main_action=="DIRECTORY"){
			//$agi->exec("Directory","voicemail,temp,f");
			$agi->hangup();
		}
		elseif($main_action=="IVR"){
			$anotherivr = 0;
			$timeoutcount = 0;$invalidcount = 0;
			$anothertimeoutcount = 0;$anotherinvalidcount = 0;
			$ivr = json_decode($ivr,true);
			open_ivr_again:
			if($welcome_enabled==1){$agi->exec("Playback",PROMPTPATH.$welcome_message);}
			if($call_record==1){$agi->exec("MixMonitor",$callid.".wav,ab");}
			
			always_open_ivr_to_ivr:
			$result = $agi->get_data(PROMPTPATH.$ivr_message, 5000, 1);
			$digit = $result['result'];
			$ivrdata = $result['data'];
			$press_key = "press".$digit;
			
			if(array_key_exists($press_key,$ivr)){
				$agi->verbose("**** $press_key *******");
				$app = $ivr[$press_key]['App'];
				$data = $ivr[$press_key]['Data'];
				
				if($app=="EXTENSION"){
					if($training_quality_enabled==1){$agi->exec("Playback",PROMPTPATH.$training_quality_message);}
					if($moh_enabled==1){
						$agi->exec("Dial","PJSIP/".$data.",".$timeout.",m(".$moh_name.")");
					}
					else{
						$agi->exec("Dial","PJSIP/".$data.",".$timeout);
					}
					
					
					$res = $agi->get_variable('DIALSTATUS');
					$resultcode = $res['result'];
					$status = $res['data'];//dialstatus checking
					$agi->verbose("**** CALL STATUS , $status AND RESULT CODE $resultcode *******");
					if($voicemail_enabled==1&&($status=="NOANSWER"||$status=="BUSY")){
						$agi->exec("Playback",PROMPTPATH.$voicemail_message);
						$agi->exec("Voicemail",$voicemail_extension."@voicemail-".$client_id.",s");
						$agi->hangup();
					}
					else{
						$agi->hangup();
					}
				}
				elseif($app=="DEPARTMENT"){
					$qdept = mysqli_query($con,"SELECT * FROM st_department WHERE id=$data");
					$rowdept = mysqli_fetch_array($qdept);
					$data = $rowdept['json_string'];
					$agi->exec("Set","CALLERID(num)=".$callee);
					if($training_quality_enabled==1){$agi->exec("Playback",PROMPTPATH.$training_quality_message);}
					if($moh_enabled==1){
						$agi->exec("Dial",$data.",".$timeout.",m(".$moh_name.")");
					}
					else{
						$agi->exec("Dial",$data.",".$timeout);
					}
					
					
					$res = $agi->get_variable('DIALSTATUS');
					$resultcode = $res['result'];
					$status = $res['data'];//dialstatus checking
					$agi->verbose("**** CALL STATUS , $status AND RESULT CODE $resultcode *******");
					if($voicemail_enabled==1&&($status=="NOANSWER"||$status=="BUSY")){
						$agi->exec("Playback",PROMPTPATH.$voicemail_message);
						$agi->exec("Voicemail",$voicemail_extension."@voicemail-".$client_id.",s");
						$agi->hangup();
					}
					else{
						$agi->hangup();
					}
				}
				elseif($app=="PHONE"){
					$agi->exec("Set","CALLERID(num)=".$callee);
					if($training_quality_enabled==1){$agi->exec("Playback",PROMPTPATH.$training_quality_message);}
					if($moh_enabled==1){
						$agi->exec("Dial","PJSIP/".$data."@mtntrunk,",$timeout.",m(".$moh_name.")");
					}
					else{
						$agi->exec("Dial","PJSIP/".$data."@mtntrunk,",$timeout);
					}
					
					
					$res = $agi->get_variable('DIALSTATUS');
					$resultcode = $res['result'];
					$status = $res['data'];//dialstatus checking
					$agi->verbose("**** CALL STATUS , $status AND RESULT CODE $resultcode *******");
					if($voicemail_enabled==1&&($status=="NOANSWER"||$status=="BUSY")){
						$agi->exec("Playback",PROMPTPATH.$voicemail_message);
						$agi->exec("Voicemail",$voicemail_extension."@voicemail-".$client_id.",s");
						$agi->hangup();
					}
					else{
						$agi->hangup();
					}
				}
				elseif($app=="MESSAGE"){$data = basename($data,".wav");$agi->exec("Playback",PROMPTPATH.$data);$agi->hangup();}
				elseif($app=="VOICEMAIL"){$agi->exec("Playback",PROMPTPATH.$voicemail_message);$agi->exec("Voicemail",$data."@voicemail-".$client_id.",s");$agi->hangup();}
				elseif($app=="QUEUE"){
					if($training_quality_enabled==1){$agi->exec("Playback",PROMPTPATH.$training_quality_message);}
					$agi->exec("Queue",$data.",Tt,,,".$timeout);
					
					$res = $agi->get_variable('QUEUESTATUS');
					$resultcode = $res['result'];
					$queue_status = $res['data'];
					if($queue_status=='TIMEOUT'&&$voicemail_enabled==1){
						$agi->exec("Playback",PROMPTPATH.$voicemail_message);
						$agi->exec("Voicemail",$voicemail_extension."@voicemail-".$client_id.",s");
						$agi->hangup();
					}
					else{
						$agi->hangup();
					}
					
				}
				elseif($app=="CONFERENCE"){
					if($training_quality_enabled==1){$agi->exec("Playback",PROMPTPATH.$training_quality_message);}
					$agi->exec("Confbridge",$data.",default_bridge,default_user,sample_user_menu");
					$agi->hangup();
				}
				elseif($app=="IVR"){
					$anotherivr = 1;
					$q = mysqli_query($con,"SELECT * FROM ivr_menus WHERE id=$data");
					$row = mysqli_fetch_array($q);
					
					$ivr_message = $row['ivr_message'];
					$ivr_message = PROMPTPATH.$ivr_message;$ivr_message = basename($ivr_message,".wav");
					$ivr = $row['ivr_json'];
					$ivr = json_decode($ivr,true);
					goto always_open_ivr_to_ivr;
				}
				else{$agi->hangup();}
				
			}
			else{
				//invalid key press ot timeout
				$agi->verbose("**** INVALID KEY OR TIMEOUT $ivrdata *******");
				if($ivrdata=="timeout"){
					if($timeoutcount>=3||$anothertimeoutcount>=3){$agi->hangup();}
					$agi->exec("Playback",PROMPTPATH.$timeout_message);
					if($anotherivr==0){$timeoutcount = $timeoutcount+1;goto open_ivr_again;}
					else{$anothertimeoutcount = $anothertimeoutcount+1;goto always_open_ivr_to_ivr;}
				}
				else{
					if($invalidcount>=3||$anotherinvalidcount>=3){$agi->hangup();}
					$agi->exec("Playback",PROMPTPATH.$invalid_message);
					if($anotherivr==0){$invalidcount = $invalidcount+1;goto open_ivr_again;}
					else{$anotherinvalidcount = $anotherinvalidcount+1;goto always_open_ivr_to_ivr;}
				}
				
			}
			
		}
		elseif($main_action=="QUEUE"){
			if($welcome_enabled==1){
				if($call_record==1){
					$agi->exec("Playback",PROMPTPATH.$welcome_message);
					if($training_quality_enabled==1){$agi->exec("Playback",PROMPTPATH.$training_quality_message);}
					$agi->exec("MixMonitor",$callid.".wav,ab");
					$agi->exec("Queue",$queue.",Tt,,,".$timeout);
					
					$res = $agi->get_variable('QUEUESTATUS');
					$resultcode = $res['result'];
					$queue_status = $res['data'];
					if($queue_status=='TIMEOUT'&&$voicemail_enabled==1){
						$agi->exec("Playback",PROMPTPATH.$voicemail_message);
						$agi->exec("Voicemail",$voicemail_extension."@voicemail-".$client_id.",s");
						$agi->hangup();
					}
					else{
						$agi->hangup();
					}
					
				}
				else{
					$agi->exec("Playback",PROMPTPATH.$welcome_message);
					if($training_quality_enabled==1){$agi->exec("Playback",PROMPTPATH.$training_quality_message);}
					$agi->exec("Queue",$queue.",Tt,,,".$timeout);
					
					$res = $agi->get_variable('QUEUESTATUS');
					$resultcode = $res['result'];
					$queue_status = $res['data'];
					if($queue_status=='TIMEOUT'&&$voicemail_enabled==1){
						$agi->exec("Playback",PROMPTPATH.$voicemail_message);
						$agi->exec("Voicemail",$voicemail_extension."@voicemail-".$client_id.",s");
						$agi->hangup();
					}
					else{
						$agi->hangup();
					}
					
				}
			}
			else{
				if($call_record==1){
					if($training_quality_enabled==1){$agi->exec("Playback",PROMPTPATH.$training_quality_message);}
					$agi->exec("MixMonitor",$callid.".wav,ab");
					$agi->exec("Queue",$queue.",Tt,,,".$timeout);
					
					$res = $agi->get_variable('QUEUESTATUS');
					$resultcode = $res['result'];
					$queue_status = $res['data'];
					$agi->verbose("QueuStatus: $queue_status VoicemailEnabled: $voicemail_enabled ..........");
					if($queue_status=='TIMEOUT'&&$voicemail_enabled==1){
						$agi->exec("Playback",PROMPTPATH.$voicemail_message);
						$agi->exec("Voicemail",$voicemail_extension."@voicemail-".$client_id.",s");
						$agi->hangup();
					}
					else{
						$agi->hangup();
					}
					
				}
				else{
					if($training_quality_enabled==1){$agi->exec("Playback",PROMPTPATH.$training_quality_message);}
					$agi->exec("Queue",$queue.",Tt,,,".$timeout);
					
					$res = $agi->get_variable('QUEUESTATUS');
					$resultcode = $res['result'];
					$queue_status = $res['data'];
					$agi->verbose("QueuStatus: $queue_status VoicemailEnabled: $voicemail_enabled ..........");
					if($queue_status=='TIMEOUT'&&$voicemail_enabled==1){
						$agi->exec("Playback",PROMPTPATH.$voicemail_message);
						$agi->exec("Voicemail",$voicemail_extension."@voicemail-".$client_id.",s");
						$agi->hangup();
					}
					else{
						$agi->hangup();
					}
					
				}
			}
		}
		elseif($main_action=="CONFERENCE"){
			if($welcome_enabled==1){
				if($call_record==1){
					$agi->exec("Playback",PROMPTPATH.$welcome_message);
					if($training_quality_enabled==1){$agi->exec("Playback",PROMPTPATH.$training_quality_message);}
					$agi->exec("MixMonitor",$callid.".wav,ab");
					$agi->exec("Confbridge",$conference.",default_bridge,default_user,sample_user_menu");
					$agi->hangup();
				}
				else{
					$agi->exec("Playback",PROMPTPATH.$welcome_message);
					if($training_quality_enabled==1){$agi->exec("Playback",PROMPTPATH.$training_quality_message);}
					$agi->exec("Confbridge",$conference.",default_bridge,default_user,sample_user_menu");
					$agi->hangup();
				}
			}
			else{
				if($call_record==1){
					if($training_quality_enabled==1){$agi->exec("Playback",PROMPTPATH.$training_quality_message);}
					$agi->exec("MixMonitor",$callid.".wav,ab");
					$agi->exec("Confbridge",$conference.",default_bridge,default_user,sample_user_menu");
					$agi->hangup();
				}
				else{
					if($training_quality_enabled==1){$agi->exec("Playback",PROMPTPATH.$training_quality_message);}
					$agi->exec("Confbridge",$conference.",default_bridge,default_user,sample_user_menu");
					$agi->hangup();
				}
			}
		}
	}
}
}
else{
	$agi->verbose("****No DID number found *******");
	$agi->hangup();
}
	
	





?>