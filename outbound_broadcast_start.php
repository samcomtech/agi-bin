#!/usr/bin/php
<?php
include('config.php');
$callid = $argv[1];
$agent = $argv[2];
$number = $argv[3];
$campaign_id = $argv[4];
$client_id = $argv[5];
$outbound_caller_id = $argv[6];
$timeout = $argv[7];
$moh_name = $argv[8];
$call_record = $argv[9];
$outbound_id = $argv[10];
$queue = $argv[11];
$welcome_greeting = $argv[12];
$press_digit = $argv[13];
$invalid_message = "invalid-speech-8khz.wav";

$welcome_greeting = PROMPTPATH.$welcome_greeting;
$welcome_greeting = basename($welcome_greeting,".wav");
$invalid_message = PROMPTPATH.$invalid_message;
$invalid_message = basename($invalid_message,".wav");

global $agi,$prompts;
require('phpagi/phpagi.php');
$agi = new AGI();

$agi->verbose("****callid is $callid agent is $agent callee is $number *******");

$call_start = date('Y-m-d H:i:s');
$q = "INSERT INTO st_call_logs (call_id,client_id,campaign_id,caller_id,callee,direction,call_start) VALUES ('$callid','$client_id','$campaign_id','$outbound_caller_id','$number','outbound','$call_start')";
mysqli_query($con,$q);

//updating is_picked_up and is_dialed status
$q = "UPDATE st_outbound SET is_picked_up=1,is_dialed=1 WHERE id=$outbound_id";
mysqli_query($con,$q);

if($call_record==1){$agi->exec("MixMonitor",$callid.".wav,ab");}
ivr_again:
$result = $agi->get_data(PROMPTPATH.$welcome_greeting, 5000, 1);
$digit = $result['result'];

if($digit==$press_digit){
	$agi->exec("Set","CALLERID(num)=".$outbound_caller_id);
	$agi->exec("Queue",$queue.",Tt,,,".$timeout);
	$agi->hangup();
}
else{
	//invalid key press
	$agi->verbose("**** INVALID KEY *******");
	$agi->exec("Playback",PROMPTPATH.$invalid_message);
	goto ivr_again;
}

?>