#!/usr/bin/php
<?php
include('config.php');
$callid = $argv[1];
$callduration = $argv[2];
$callstatus = $argv[3];
$callrecording = $argv[4];$callrecording = explode(":",$callrecording);$callrecording = $callrecording[1];
$dialstatus = $argv[5];$dialstatus = explode(":",$dialstatus);$dialstatus = $dialstatus[1];
$destination = $argv[6];$destination = explode(":",$destination);$destination = $destination[1];
if($destination!=""){$destination = explode("/",$destination);$destination = explode("-",$destination[1]);$destination = $destination[0];}
else{$destination="";}
$lastapp = $argv[7];$lastapp = explode(":",$lastapp);$lastapp = $lastapp[1];
$lastdata = $argv[8];$lastdata = explode(":",$lastdata);$lastdata = $lastdata[1];
$call_end = date('Y-m-d H:i:s');

if($callrecording!=""){
	$callrecording=basename($callrecording);
	$callrecfile = CALLRECORDINGPATH.$callrecording;
	$callrecmovefile = CALLRECORDINGMOVEPATH;
	`mv $callrecfile $callrecmovefile`;
}
else{
	$callrecording='';
}

global $agi,$prompts;
require('phpagi/phpagi.php');
$agi = new AGI();

$agi->verbose("****CallID is $callid CallStatus is $dialstatus CallDuration is $callduration *******");
$q = "UPDATE st_call_logs SET call_duration='$callduration',call_status='$dialstatus',call_recording='$callrecording',destination='$destination',last_app='$lastapp',last_data='$lastdata',call_end='$call_end' WHERE call_id='$callid'";
$agi->verbose("CALL END QUERY: $q ");
mysqli_query($con,$q);

//call duration update
$q = mysqli_query($con,"SELECT * FROM st_call_logs WHERE call_id='$callid'");
$row = mysqli_fetch_array($q);
$call_start = new DateTime($row['call_start']);
$call_end = new DateTime($row['call_end']);

$interval = $call_start->diff($call_end);

$hr = $interval->format("%H");
$min = $interval->format("%i");
$sec = $interval->format("%s");

$callduration = $hr*3600 + $min*60 + $sec;
$q = "UPDATE st_call_logs SET call_duration='$callduration' WHERE call_id='$callid'";
$agi->verbose("CALLDURATION UPDATE QUERY: $q ");
mysqli_query($con,$q);

?>
