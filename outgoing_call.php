#!/usr/bin/php
<?php
include('config.php');
$callid = $argv[1];
$callerid = $argv[2];
$callee = $argv[3];

global $agi,$prompts;
require('phpagi/phpagi.php');
$agi = new AGI();

$agi->verbose("****callid is $callid caller id is $callerid callee is $callee *******");

//check calling extension is available in database or not
$q = mysqli_query($con,"SELECT * FROM ps_endpoints WHERE id='$callerid'");
$n_rows = mysqli_num_rows($q);
if($n_rows>0){
	$row = mysqli_fetch_array($q);
	$user_id = $row['user_id'];
	$client_id = $row['client_id'];
	$tracking_source_id = 0;
	$timeout = 30;
	$outbound_caller_id = $row['caller_id'];
	$dial_out_allowed = $row['dial_out_allowed'];
	$recording_enabled = $row['recording_enabled'];
}
else{
	//calling extension number not found
	$agi->hangup();
}

$dial_len = strlen($callee);
//INTERNAL SIP TO SIP CALLING
if($dial_len<11){
	//dial internal sip to sip call
	$call_start = date('Y-m-d H:i:s');
	$q = "INSERT INTO st_call_logs (call_id,user_id,client_id,tracking_source_id,caller_id,callee,direction,call_start) VALUES 
	('$callid',$user_id,$client_id,$tracking_source_id,$callerid,$callee,'outbound','$call_start')";
	$agi->verbose("INITIAL CALL LOG QUERY: $q ");
	mysqli_query($con,$q);
	
	if($recording_enabled==1){
		$agi->exec("MixMonitor",$callid.".wav,ab");
		$agi->exec("Dial","PJSIP/".$client_id.$callee.",".$timeout);
		$agi->hangup();
	}
	else{
		$agi->exec("Dial","PJSIP/".$client_id.$callee.",".$timeout);
		$agi->hangup();
	}
	
}
//dial outside to external number
else{
	if($dial_out_allowed==1){
	//initial call log query
	$call_start = date('Y-m-d H:i:s');
	$q = "INSERT INTO st_call_logs (call_id,user_id,client_id,tracking_source_id,caller_id,callee,direction,call_start) VALUES 
	('$callid',$user_id,$client_id,$tracking_source_id,$callerid,$callee,'outbound','$call_start')";
	$agi->verbose("INITIAL CALL LOG QUERY: $q ");
	mysqli_query($con,$q);
	
	if($recording_enabled==1){
		$agi->exec("MixMonitor",$callid.".wav,ab");
		$agi->exec("Set","CALLERID(num)=".$outbound_caller_id);
		$agi->exec("Dial","PJSIP/".$callee."@mtntrunk,".$timeout);
		$agi->hangup();
	}
	else{
		$agi->exec("Set","CALLERID(num)=".$outbound_caller_id);
		$agi->exec("Dial","PJSIP/".$callee."@mtntrunk,".$timeout);
		$agi->hangup();
	}
	
	}
	else{
		$agi->verbose("**** YOU ARE NOT ALLOWED TO DIAL OUT *******");
		$agi->hangup();
	}
}



?>